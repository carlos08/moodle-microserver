using System.Security.Claims;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var authenticationProviderKey = "TestKey";


var builder = WebApplication.CreateBuilder(args);


builder.Configuration.AddJsonFile("configuration.json",optional:false,reloadOnChange:true);

/*builder.Services.AddAuthentication().
AddJwtBearer().AddJwtBearer("jwtDevelopment");
*/
//builder.Services.AddAuthentication("Bearer").AddJwtBearer(authenticationProviderKey);



/*builder.Services.AddAuthentication(authenticationProviderKey).AddJwtBearer( authenticationProviderKey,jwtopt =>{
    jwtopt.Audience="http://localhost:5183/api/user/select";
    jwtopt.Authority="dotnet-user-jwts";
    
   // jwtopt.Audience="test";
   jwtopt.RequireHttpsMetadata=false;
});
*/
builder.Services.AddOcelot(builder.Configuration);

    
var app = builder.Build();

app.UseAuthorization();

app.MapControllers();

await app.UseOcelot();



app.Run();
