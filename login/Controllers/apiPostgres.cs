using System;
using Microsoft.AspNetCore.Mvc;
using Npgsql;

[ApiController]
[Route("Apipostgres")]
public class apiPosgres : ControllerBase
{

    private readonly ServicesUser _serviceUser;
    private readonly ILogger<apiPosgres> _logger;

    private static string url_upload_ing = "/home/lenovo/proyectos_c#/img";



    public apiPosgres(ServicesUser servicesUser, ILogger<apiPosgres> logger)
    {
        _serviceUser = servicesUser;
        _logger = logger;
    }


    [HttpPost]
    [Route("/Login")]
    public IActionResult Login([FromBody] Modelusuario user)
    {



        return Ok(_serviceUser.Token(user));
    }

    [HttpPost]
    [Route("/user")]
    public IActionResult Example(Modelusuario user)
    {

        var result = _serviceUser.CreateUSer(user);

        return Created("200",result);

    }
    [HttpGet]
    [Route("/select")]
    public IActionResult usuario()
    {

            

        return Ok(_serviceUser.SelectUser());
    }

    [HttpPost]
    [Route("/upload/imagen")]
    public async Task<IActionResult> UploadImagen([FromForm] ModelImagen modelimagen)
    {
        string mensajeExection = null;
        if (ValidImagen(modelimagen.imagen))
        {

            try
            {
                //Path.Combine(Path.GetDirectoryName(imagen.name));
                var igmg = Path.Combine(url_upload_ing, Path.GetRandomFileName());

                using (var stream = System.IO.File.Create(igmg))
                {
                    await modelimagen.imagen.CopyToAsync(stream);
                    _logger.LogDebug("Se subio correctamente la foto");
                    mensajeExection = "Archivo subivo correctamente";
                }
            }
            catch
            {
                _logger.LogError("No se puede subir archivo");
            }
        }
        else
        {
            Console.WriteLine("Archivo no valido");
            mensajeExection = "Archivo no valido";
        }

        return Ok(mensajeExection);
    }



    private bool ValidImagen(IFormFile imagen)
    {

        var Valid_Exten = new[] { ".jpg", ".png", ".jpg" };


        var extencion = Path.GetExtension(imagen.FileName).ToLower();



        return Valid_Exten.Contains(extencion);
    }
}


