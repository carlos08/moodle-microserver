

using System.Data;
using Npgsql;

public class ServicesUser
{

    public readonly ServidorDb _ServidorDb;

    private NpgsqlConnection _connection;

    private Object _listUser;
    private List<Object> _datos;

    public ServicesUser(ServidorDb servidorDb)
    {
        _ServidorDb = servidorDb;


    }
    public bool CreateUSer(Modelusuario user)
    {
        using (var connection = _ServidorDb.DbConnectionPosgrest())
        {
            _connection = connection;
            using (NpgsqlCommand command = new NpgsqlCommand())
            {
                command.Connection = _connection;
                command.CommandText = "INSERT INTO usuario(id,nombre,password) VALUES(:id,:nombre,:password)";
                command.Parameters.AddWithValue("iduserº", user.id);
                command.Parameters.AddWithValue("nombre", user.nombre);
                command.Parameters.AddWithValue("password", user.Password);
                var result = command.ExecuteNonQuery();
                return true;

            }
        }
        return false;
    }
    public dynamic Token(Modelusuario _usuario)
    {
        try
        {
            using (_connection = _ServidorDb.DbConnectionPosgrest())
            {


                using (var command = new NpgsqlCommand())
                {
                    command.Connection = _connection;
                    command.CommandText = "SELECT * FROM usuario";



                    using (var result = command.ExecuteReader())
                    {

                        bool Enconstrado = false;

                        while (result.Read())
                        {
                            if (_usuario.Password == result.GetString(2) &&
                                _usuario.usuario == result.GetString(1))
                            {
                                _listUser = new
                                {
                                    token = "6Nsudb7q3i78IYWE87OQT.32R423UIR832RIO32R.32DIU32YUBYUI32YR3223R.8293BE92378932N9"
                                };

                                break;
                            }

                            if (!Enconstrado)
                            {
                                _listUser = new
                                {

                                    mensaje = "El correo o la constraseñas incorectos"
                                };
                            }


                        }
                    }

                }
                _connection.Close();

            }
        }
        catch
        {

            _listUser = new
            {

                mensaje = "Espere unos minutos para solucionar nuestros problemas"
            };
        }

        return (_listUser);
    }

    public object? SelectUser()
    {

        try
        {
            _datos = new();
            using (var conn = _ServidorDb.DbConnectionPosgrest())
            {


                using (NpgsqlCommand command = new NpgsqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "SELECT * FROM usuario";
                    var result = command.ExecuteReader();


                    while (result.Read())
                    {
                        _listUser = new
                        {
                            id = result.GetString(0),
                            nombre = result.GetString(1),
                            passsowod = result.GetString(2),
                        };

                        _datos.Add(_listUser);
                    }
                }



            }
        }
        catch (Exception e) {
            _datos.Add("Por el momento no podemos procesar su solcitud, Intentelo mas tarde. Gracias!");
        }

        return _datos;

        }
    }