
using System;
using Npgsql;

public class ConexionPostgres : ServidorDb
{

    private List<DataConnectionDB> listConexion;

    private readonly ILogger<ConexionPostgres> _logger;

    private   NpgsqlConnection connectionP;
    public ConexionPostgres(ILogger<ConexionPostgres>  logger){
        _logger=logger;
        
  
        listConexion = new List<DataConnectionDB>{
            new DataConnectionDB("localhost", "postgres", "mipassword", 0, "usuario"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassworwd", 0, "postgres"),

            new DataConnectionDB("localhost", "postgres", "mipassword", 0, "postgres"),
         };




        int cont = 1;


        foreach (DataConnectionDB connection in listConexion)
        {

            try
            {
                var connString = $"Host={connection.Host};Username={connection.Username};Password={connection.Password};Database={connection.Database}";
                  connectionP =new NpgsqlConnection(connString);

                        connectionP.Open();
                        
               // Console.WriteLine($" conexion -> {connection.Database} ");

                break;
            }
            catch (Exception)
            {


                string mensajeError = (cont == listConexion.Count()) ?   "Hay que llamar al administrador para que chece las conexion de la base  de datos":$"Error en la conexion -> {connection.Host} posicion {cont}";

                
                _logger.LogError(mensajeError);


            }
            
            cont++;

        }

    }


    public NpgsqlConnection DbConnectionPosgrest()=> connectionP;






}

