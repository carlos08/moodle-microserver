

public class DataConnectionDB{

private  string host;
private  string username;
private  string password;
private  int  port;
private  string database;


public DataConnectionDB(string _host,string _username,string _password,int _port,string _database){

host=_host;
username=_username;
password=_password;
port=_port;
database=_database;

}

public string Host { get {return host;} set{host=value;} }
public string Username  { get {return username;} set{username=value;} }
public string Password  { get {return password;} set{password=value;} }
 public int  Port  { get {return port;} set{port=value;} }
public string Database  { get {return database;} set{database=value;} }

}